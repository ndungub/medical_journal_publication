REQUIREMENTS:
1. Make sure maven2 in installed in your compiling machine.
   https://maven.apache.org/docs/2.2.1/release-notes.html
   
2. The database of choice is MySQL, the database script is included under root project folder. 
   Run it normally like any other MySQL database script.
   
3. Spring was the technology of choice, it's largely supported and by used og hibernate on can manage
   database transactions optimally even in a case where the dateset get large.

4. Completion of test not covered fully, Spring MCV requires quite amount of time to setup.
   PDF reader for the desktop application is a challenge consinderind there are no stable open source libraries.
 
5. To compile the project, under root project folder run:
   1.cd Medical_Journal_Publication
   2. mvn clean
   (For initial test)
   3. mvn package -Pfe
   (Subsquent runs)
   4. mvn package
   
   To compilte the desktop application
   1. cd JournalReader
   2. mvn clean
   3. mvn package
   
6. Issues faced include lack of enough time to complete the project since I'm already working in other company tasks.
   
7. None, quite challenging.