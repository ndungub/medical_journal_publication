/* Create authentication table*/
create table auth
(
 auth_username VARCHAR(36) not null,
 auth_pwd VARCHAR(36) not null,
 auth_role     VARCHAR(36) not null,
 PRIMARY KEY(auth_username)
) ;
 
insert into auth (auth_username, auth_pwd, auth_role)
values ('boniface', 'boniface', 'PUBLISHER');
 
insert into auth (auth_username, auth_pwd, auth_role)
values ('crossover', 'crossover', 'NON_PUBLISHER');


/* Create publication  table*/
create table publications
(
 journal_id INT AUTO_INCREMENT,
 journal_name VARCHAR(100) not null,
 publication_date DATETIME not null,
 auth_username     VARCHAR(36) not null,
 PRIMARY KEY(journal_id),
 UNIQUES(journal_name,auth_username),
 FOREIGN KEY (auth_username) 
        REFERENCES auth(auth_username)
) ;

INSERT INTO publications (journal_name,publication_date,auth_username) VALUES('The Cancer Within and Without',NOW(),'boniface');
INSERT INTO publications (journal_name,publication_date,auth_username) VALUES('The Day the ER broke',NOW(),'boniface');
INSERT INTO publications (journal_name,publication_date,auth_username) VALUES('Drugs manual',NOW(),'boniface');
INSERT INTO publications (journal_name,publication_date,auth_username) VALUES('Medicine 101',NOW(),'boniface');
INSERT INTO publications (journal_name,publication_date,auth_username) VALUES('Radiology Explained',NOW(),'boniface');


/* Create subscriber  table*/
create table subscribers
(
 subscriber_id INT AUTO_INCREMENT,
 journal_id INT not null,
 subscribed_on DATETIME not null,
 auth_username     VARCHAR(36) not null,
 PRIMARY KEY(subscriber_id),
 UNIQUE(journal_id),
  FOREIGN KEY (journal_id)
        REFERENCES publications(journal_id)
          ON DELETE CASCADE,
 FOREIGN KEY (auth_username) 
        REFERENCES auth(auth_username)
) ;



INSERT INTO subscribers (journal_id,subscribed_on,auth_username) VALUES(1,NOW(),'crossover');
INSERT INTO subscribers (journal_id,subscribed_on,auth_username) VALUES(2,NOW(),'crossover');
INSERT INTO subscribers (journal_id,subscribed_on,auth_username) VALUES(3,NOW(),'crossover');
INSERT INTO subscribers (journal_id,subscribed_on,auth_username) VALUES(4,NOW(),'crossover');


