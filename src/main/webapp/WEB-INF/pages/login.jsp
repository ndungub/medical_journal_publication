<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <link href="<c:url value="/resources/js/bootstrap/dist/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/signin.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery/dist/jquery.min.js" />"></script>

    
    <title>Login</title>
</head>

<body>

<!-- Lofin container -->
<div class="container col-sm-offset-3 col-sm-6" >

      <form class="form-signin"  name='f' action="security_check" method='POST'>
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputUser" class="sr-only">User</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="" autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="">
        <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Sign in</button>
      </form>

    </div>
</body>
</html>