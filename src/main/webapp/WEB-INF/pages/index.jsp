<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <link href="<c:url value="/resources/js/bootstrap/dist/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery/dist/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap/dist/js/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/publisher.js" />"></script>
    
    <title>Publisher</title>
    
    <style type="text/css">
	    .fileupload{
		    position:absolute;
		    z-index:2;
		    top:0;
		    left:0;
		    filter: alpha(opacity=0);
		    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		    opacity:0;
		    background-color:transparent;
		    color:transparent;
	    }
	  
    </style>
    
     <script type="text/javascript">
	     var model=[];
	     model.username="${pageContext.request.userPrincipal.name}";
     </script>
     
</head>
<body>
  
  
<div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">

          <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#viewpublication" id="linkviewpublication">View/Manage Publication</a></li>
            <li><a href="#addpublication" id="linkaddpublication">Add Publication</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <c:if test="${pageContext.request.userPrincipal.name != null}">${pageContext.request.userPrincipal.name}
			  </c:if> 
  
  			 <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<c:url value="/logout" />" >Logout</a></li>
              </ul>
            </li>
          </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

   <div class="row">
		<div class="panel panel-default" id="pnviewpublication">
		  <div class="panel-heading">
		    <h3 class="panel-title">View/Manage Publications</h3>
		  </div>
		  <div class="panel-body">
	   	 		<table class="table table-hover table-striped table-bordered" id="tbviewjournals" >
			        <thead>
			            <tr>
			                <th>#</th>
			                <th class="hide">Journal ID</th>
			                <th>Journal Name</th>
			                <th>Uploaded On</th>
			                <th>#Subscribers</th>
			                <th></th>
			            </tr>
			        </thead>
			        <tbody>
	
			        </tbody>
		       </table>	
		       
		       <button id="deletepublication" class="btn btn-lg btn-primary btn-block"  type="button">Delete Publication</button>
		  </div>
		</div>
		
		<div class="panel panel-default hide" id="pnaddpublication">
		  <div class="panel-heading">
		    <h3 class="panel-title">UploadPublications</h3>
		  </div>
		  <div class="panel-body">
		  

		   <form method="post" enctype="multipart/form-data" action="uploadjournal">
				<div style="position:relative;">
					<a class='btn btn-primary' href='javascript:;'>
						Choose File...
					<input type="file" class="fileupload" name="file" accept="application/pdf"  required size="40" onchange='$("#upload-file-info").html($(this).val());'  required>
					</a>
						&nbsp;
						<span class='label label-info' id="upload-file-info"></span>
						
					
				</div>
					
				<div class="input-group">
				  <span class="input-group-addon">Filename</span>
				  <input type="text" class="form-control" name="customfilename" >
				  <span class="input-group-addon">.PDF</span>
				</div>
				<button type="submit" class="btn btn-default">Upload Publication</button>
			</form>
		  </div>
		</div>
	</div>


    </div>
    

</body>
</html>