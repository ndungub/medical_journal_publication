var baseUrl="http://" + location.host + "/Medical_Journal_Publication";

$(document).ready(function() {

	//change menu for subscriber
	$('#linkviewsubscription').on('click', function() {
	    $('#pnviewsubscription').removeClass('hide');
	    
	  });
	
	//add subscription
	$('#btnsubscribe').on('click', function() {
		addSubscriptions();
	  });
	
	//delete subscription
	$('#deletesubscriptions').on('click', function() {
		deleteSubscriptions();
	  });
	
	//get subscription
	getSubscriptions();
	
	//get all publications
	getAllPublications().done(function(result) {
		$.each(result, function(i, value) {
			$('#selectpublications').append($('<option>', { 
		        value: value.journal_id,
		        text : value.journal_name 
		    }));
		});
	});
	
});

//AJAX requests
getAllPublications = function() {

	  return $.ajax({
		    type: "POST",
		    contentType : "application/json",
		    url: baseUrl + "/allpublications",
		    dataType: "json", 
		    success: function(result) {
		      return result;
		    }
		  });
	};
	
getSubscriptions = function() {
	var data ={};
	data['auth_username']=model.username;

	  return $.ajax({
		    type: "POST",
		    contentType : "application/json",
		    url: baseUrl + "/subscriptions",
		    data : JSON.stringify(data),
		    dataType: "json", 
		    success: function(result) {
		    	$("#tbviewsubscription > tbody").html("");

		    	$.each(result, function(i, value) {
		    		checkOption = $('<input type="checkbox" name="subscriptions" value="">');
		    		$('#tbviewsubscription').find('tbody').append($('<tr>').append($('<td>').text(i+1)).append($('<td class="hide">').text(value.journal_id)).append($('<td>').text(value.journal_name)).append($('<td>').text(value.subscribed_on)).append($('<td>').append(checkOption)));
		    	});
		    	
		      return result;
		    }
		  });
	};

	addSubscriptions = function() {
		var data ={};
		data['auth_username']=model.username;
		data['journal_id']=$("#selectpublications option:selected").val();

		  return $.ajax({
			    type: "POST",
			    contentType : "application/json",
			    url: baseUrl + "/addsubscription",
			    data : JSON.stringify(data),
			    dataType: "json", 
			    success: function(result) {	
			    	if(result.message =="success"){
			    		getSubscriptions();
			    	}else{
			    		if(result.error==1062){
			    			alert("You have already subscribed to this journal");
			    		}
			    	}
			    }
			  });
		};
		
deleteSubscriptions = function() {
	var data ={};
	data['auth_username']=model.username;
	var journal_ids=[];
	
	$('#tbviewsubscription tr').each(function(i,elem) {
		if(i !=0){
			if($(this).find('td:eq(4)').find('input[type=checkbox]').is(':checked')){
				journal_ids.push($(this).find('td:eq(1)').text());
			}
		}
		
	});
	
	data['journal_ids']=journal_ids;

	  return $.ajax({
		    type: "POST",
		    contentType : "application/json",
		    url: baseUrl + "/deletesubscriptions",
		    data : JSON.stringify(data),
		    dataType: "json", 
		    success: function(result) {	
		    	if(result =="success"){
		    		getSubscriptions();
		    	}
		    }
		  });
	};