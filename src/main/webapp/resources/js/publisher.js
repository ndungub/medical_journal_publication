//Set base URL
var baseUrl="http://" + location.host + "/Medical_Journal_Publication";

$(document).ready(function() {

	//change menu
	$('#linkviewpublication').on('click', function() {
		$('#pnaddpublication').addClass('hide');
	    $('#pnviewpublication').removeClass('hide');
	    
	  });
	
	//add publications
	$('#linkaddpublication').on('click', function() {
		$('#pnviewpublication').addClass('hide');
	    $('#pnaddpublication').removeClass('hide');
	  });

	//delete publications
	$('#deletepublication').on('click', function() {
		deletePublications();
	  });
	
	//callback loading of publications
	getPublications().done(function(result) {
    	$("#tbviewjournals > tbody").html("");

    	$.each(result, function(i, value) {
    		checkOption = $('<input type="checkbox" name="publication" value="">');
    		$('#tbviewjournals').find('tbody').append($('<tr>').append($('<td>').text(i+1)).append($('<td class="hide">').text(value.journal_id)).append($('<td>').text(value.journal_name)).append($('<td>').text(value.publication_date)).append($('<td>').append(checkOption)));
    	});
	});
	
});

//Ajax requests
getPublications = function() {
	var data ={};
	data['auth_username']=model.username;

	  return $.ajax({
		    type: "POST",
		    contentType : "application/json",
		    url: baseUrl + "/publications",
		    data : JSON.stringify(data),
		    dataType: "json", 
		    success: function(result) {
		      return result;
		    }
		  });
	};

deletePublications = function() {
	var data ={};
	data['auth_username']=model.username;
	var journal_ids=[];
	
	$('#tbviewjournals tr').each(function(i,elem) {
		if(i !=0){
			if($(this).find('td:eq(4)').find('input[type=checkbox]').is(':checked')){
				journal_ids.push($(this).find('td:eq(1)').text());
			}
		}
		
	});
	
	data['journal_ids']=journal_ids;

	  return $.ajax({
		    type: "POST",
		    contentType : "application/json",
		    url: baseUrl + "/deletepublications",
		    data : JSON.stringify(data),
		    dataType: "json", 
		    success: function(result) {	
		    	if(result =="success"){
		    		getPublications();
		    	}
		    }
		  });
	};