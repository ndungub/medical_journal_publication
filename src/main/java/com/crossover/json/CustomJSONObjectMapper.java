package com.crossover.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
/**
 * Created by boniface on 17/04/2016.
 * 
 */


//Mapping correspong JSON objects
public class CustomJSONObjectMapper extends ObjectMapper
{
    public CustomJSONObjectMapper()
    {
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
        configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
        //setSerializationInclusion(JsonSerialize.Inclusion.NON_EMPTY);

        //configure(SerializationConfig.WRITE_NULL_MAP_VALUES, false);
    }
}