package com.crossover.dto;

import java.io.Serializable;

/**
 * Created by boniface on 17/04/2016.
 * 
 */

//Publication object to map to corresponding JSON template
@SuppressWarnings("serial")
public class Publication implements Serializable{
	private int journal_id;
	private String journal_name;
	private String auth_username;
	private String publication_date;
	private int[] journal_ids; 
	
	public int[] getJournal_ids() {
		return journal_ids;
	}
	public void setJournal_ids(int[] journal_ids) {
		this.journal_ids = journal_ids;
	}
	public String getAuth_username() {
		return auth_username;
	}
	public void setAuth_username(String auth_username) {
		this.auth_username = auth_username;
	}
	
	public int getJournal_id() {
		return journal_id;
	}
	public void setJournal_id(int journal_id) {
		this.journal_id = journal_id;
	}
	public String getJournal_name() {
		return journal_name;
	}
	public void setJournal_name(String journal_name) {
		this.journal_name = journal_name;
	}
	public String getPublication_date() {
		return publication_date;
	}
	public void setPublication_date(String publication_date) {
		this.publication_date = publication_date;
	}

	
}
