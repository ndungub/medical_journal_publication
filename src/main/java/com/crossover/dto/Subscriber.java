package com.crossover.dto;

import java.io.Serializable;

/**
 * Created by boniface on 17/04/2016.
 * 
 */

//Publication object to map to corresponding JSON template
@SuppressWarnings("serial")
public class Subscriber implements Serializable{
	private int subscriber_id;
	private int journal_id;
	private String subscribed_on;
	private String auth_username;
	private String journal_name;
	private int[] journal_ids;
	


	
	public int getSubscriber_id() {
		return subscriber_id;
	}
	public void setSubscriber_id(int subscriber_id) {
		this.subscriber_id = subscriber_id;
	}
	public String getSubscribed_on() {
		return subscribed_on;
	}
	public void setSubscribed_on(String subscribed_on) {
		this.subscribed_on = subscribed_on;
	}
	public String getAuth_username() {
		return auth_username;
	}
	public void setAuth_username(String auth_username) {
		this.auth_username = auth_username;
	}
	public String getJournal_name() {
		return journal_name;
	}
	public void setJournal_name(String journal_name) {
		this.journal_name = journal_name;
	}
	public int getJournal_id() {
		return journal_id;
	}
	public void setJournal_id(int journal_id) {
		this.journal_id = journal_id;
	}
	public int[] getJournal_ids() {
		return journal_ids;
	}
	public void setJournal_ids(int[] journal_ids) {
		this.journal_ids = journal_ids;
	}

	
	

}
