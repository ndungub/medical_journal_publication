package com.crossover.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.crossover.dto.Publication;
import com.crossover.dto.Subscriber;
import com.crossover.model.DataAccess;

/**
 * Created by boniface on 17/04/2016.
 * 
 */


@Controller
public class JournalController {
	//Autowire Data access class instead of initializing everytime
	@Autowired
	private DataAccess dataAccess;

	  //Requesting mapping and redirection to appropriate views
	  //Root mapping to login 
	//Get all publications
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/allpublications", method = RequestMethod.POST)
	  public  List getAllPublications() {
		
		
		return dataAccess.getPublications();
	     
	  }
	
	//Get  publications based publisher
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/publications", method = RequestMethod.POST)
	  public  List getPublications(@RequestBody Publication pub) {
		
		return dataAccess.getPublications(pub.getAuth_username());
	     
	  }
	 
	//delete selected publications
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/deletepublications", method = RequestMethod.POST)
	  public  List deletePublications(@RequestBody Publication pub) {
		
		List<Object> listObj =new ArrayList<Object>();
		
		if(dataAccess.deletePublications(pub.getAuth_username(),pub.getJournal_ids())){
			listObj.add("success");
		}else{
			listObj.add("error");
		}
		
		return listObj;
	    
		
	  }
	
	//Get  subscription per subscriber
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/subscriptions", method = RequestMethod.POST)
	  public  List getSubscription(@RequestBody Subscriber sub) {
		
		return dataAccess.getSubscriptions(sub.getAuth_username());
	     
	  }

	//add new subsription
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/addsubscription", method = RequestMethod.POST)
	  public  Map<String, Object> addSubscriptionss(@RequestBody Subscriber sub) {
		
		return dataAccess.Addsubscription(sub.getAuth_username(),sub.getJournal_id());
	  }
	
	//delete selected subscription
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/deletesubscriptions", method = RequestMethod.POST)
	  public  List deleteSubscriptionss(@RequestBody Subscriber sub) {
		
		List<Object> listObj =new ArrayList<Object>();
		
		if(dataAccess.deleteSubscriber(sub.getAuth_username(),sub.getJournal_ids())){
			listObj.add("success");
		}else{
			listObj.add("error");
		}
		
		return listObj;
	    
		
	  }
	
	//Upload images to upload folder
    @RequestMapping(value="/uploadjournal", method=RequestMethod.POST )
    public @ResponseBody String savePDF(@RequestParam("file") MultipartFile file, @RequestParam("customfilename") String customfilename, Principal principal ){
    	
    	Map<String, Object> listObj = new HashMap<String, Object>();
    	String fileName = null;
    	if (!file.isEmpty()) {
            try {
            	//update database
            	listObj= dataAccess.AddPublication(principal.getName(), customfilename);
            	
            	if(listObj.get("message").equals("success")){
            		//check if folder upload exists first
            		Path path = Paths.get(System.getProperty("catalina.base")+"/webapps/uploads");
            		if (!Files.exists(path)) {
            			try {
            				path = Paths.get(System.getProperty("catalina.base")+"/webapps/uploads");
            				Files.createDirectory(path);

                        } catch (IOException e) {
                            //fail to create directory
                            e.printStackTrace();
                        }
            		}
            		
	            	fileName = listObj.get("journal_id").toString() +"_" + principal.getName() +".pdf";
	                byte[] bytes = file.getBytes();
	                
	                BufferedOutputStream buffStream = 
	                        new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("catalina.base") +"/webapps/uploads/" + fileName)));
	                buffStream.write(bytes);
	                buffStream.close();
            	}
             
                
                
            } catch (Exception e) {
            	//On error return error object
                e.printStackTrace();
                listObj.put("message", "error");
                listObj.put("error", "couldn't upload file");
                
            }
        } else {
            listObj.put("message", "error");
            listObj.put("error", "couldn't upload file");
        }
    	
    	if(listObj.get("message").equals("success")){
    		 return "redirect:publisher";
    	}else{
    		return (String) listObj.get("error");
    	}
		
    }

	 
}
