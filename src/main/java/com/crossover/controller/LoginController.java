package com.crossover.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
/**
 * Created by boniface on 17/04/2016.
 * 
 */

@Controller
public class LoginController {

  //Requesting mapping and redirection to appropriate views
  //Root mapping to login 
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String defaultPage(Model model) {
    return "login";
  }
  
  //Login mapping
  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String loginPage(Model model) {
      return "login";
  }
 
  //Map to publisher on lofin success
  @RequestMapping(value = "/publisher", method = RequestMethod.GET)
  public String publisherPage(Model model) {
      return "index";
  }
  
  //Map to subscriber on login
  @RequestMapping(value = "/subscriber", method = RequestMethod.GET)
  public String nonpublisherPage(Model model, Principal principal) {
      return "subscriber";
  }
  
  //Map to 403 page for non authorised url access
  @RequestMapping(value = "/403", method = RequestMethod.GET)
  public String accessDenied(Model model, Principal principal) {
      model.addAttribute("title", "Access Denied!");
      
      if (principal != null) {
          model.addAttribute("message", "Hi " + principal.getName()
                  + "<br> You do not have permission to access this page!");
      } else {
          model.addAttribute("msg",
                  "You do not have permission to access this page!");
      }
      return "403";
  }
}
