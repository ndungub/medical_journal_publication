package com.crossover.model;

import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import antlr.StringUtils;

import com.crossover.dto.Publication;
import com.crossover.dto.Subscriber;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by xb on 17/04/2016.
 * Handles data manupulation
 */

public class DataAccess implements Serializable{
	protected static final Logger log = Logger.getLogger(DataAccess.class);

    private DataSource dataSource;
    
    //Initialize data source
	public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
	}
	
	//Get all publications return publication list
	public List<Publication> getPublications() {
	
		List<Publication> pubs = new ArrayList<Publication>();
		
		Query q = null;
		String query="SELECT journal_id,journal_name,publication_date FROM publications";

		
		try {
			
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Publication pub = new Publication();
				pub.setJournal_id(rs.getInt(1));
				pub.setJournal_name(rs.getString(2));
				pub.setPublication_date(rs.getString(3));
				
				pubs.add(pub);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pubs;
	}
	
	//Get publications per publisher and return publication list
	//Overrides all publications
	public List<Publication> getPublications(String username) {
		//dataAccess.setDataSource(dataSource);
		List<Publication> pubs = new ArrayList<Publication>();
		
		Query q = null;
		String query="SELECT journal_id,journal_name,publication_date FROM publications WHERE auth_username='"+ username +"'";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Publication pub = new Publication();
				pub.setJournal_id(rs.getInt(1));
				pub.setJournal_name(rs.getString(2));
				pub.setPublication_date(rs.getString(3));
				
				pubs.add(pub);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pubs;
	}

	//add new publications 
	public Map<String, Object> AddPublication(String username,String journal_name) {
		HashMap<String, Object> listObj = new HashMap<String, Object>();

		String query="INSERT INTO publications (journal_name,publication_date,auth_username) VALUES('"+ journal_name +"',NOW(),'"+ username +"');";

		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int lastID=0;
            if (rs != null && rs.next()) {
            	lastID = rs.getInt(1);
            }
            
            listObj.put("message", "success");
            listObj.put("journal_id", lastID);
			
		} catch (SQLException e) {
			log.error("Error adding publication.", e);
            e.printStackTrace();
            listObj.put("message", "error");
            listObj.put("error", e.getErrorCode());
		}
		
		return listObj;
		

	}
	
	//delete selected publications
	public boolean deletePublications(String username, int[] journal_ids) {
		String journal_id=Arrays.toString(journal_ids);
		journal_id=journal_id.substring(1,journal_id.length()-1);
		
		String query="DELETE FROM publications WHERE auth_username='"+ username +"' AND Journal_id IN (" + journal_id +")";

		
		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            return true;
			
			
		} catch (SQLException e) {
			log.error("Error deleting publication.", e);
            e.printStackTrace();
            return false;
		}
		

	}
	
	//Get subscription list per subscriber
	public List<Subscriber> getSubscriptions(String username) {
		
		List<Subscriber> subs = new ArrayList<Subscriber>();
		
		Query q = null;
		String query="SELECT A.subscriber_id,A.journal_id,B.journal_name,A.subscribed_on,A.auth_username FROM subscribers A, Publications B WHERE A.journal_id=B.journal_id AND A.auth_username='"+ username +"'";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Subscriber sub = new Subscriber();
				sub.setSubscriber_id(rs.getInt(1));
				sub.setJournal_id(rs.getInt(2));
				sub.setJournal_name(rs.getString(3));
				sub.setSubscribed_on(rs.getString(4));
				sub.setAuth_username(rs.getString(5));
				
				subs.add(sub);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return subs;
	}
	//Add subscription
	public Map<String, Object> Addsubscription(String username, int journal_id) {
		HashMap<String, Object> listObj = new HashMap<String, Object>();

		String query="INSERT INTO subscribers (journal_id,subscribed_on,auth_username) VALUES("+journal_id+",NOW(),'"+ username +"')";

		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            
            listObj.put("message", "success");
			
		} catch (SQLException e) {
			log.error("Error adding subscriptions.", e);
            e.printStackTrace();
            listObj.put("message", "error");
            listObj.put("error", e.getErrorCode());
		}
		
		return listObj;
		

	}
	//delete subscription
	public boolean deleteSubscriber(String username, int[] journal_ids) {
		String journal_id=Arrays.toString(journal_ids);
		journal_id=journal_id.substring(1,journal_id.length()-1);
		
		String query="DELETE FROM subscribers WHERE auth_username='"+ username +"' AND Journal_id IN (" + journal_id +")";

		
		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            return true;
			
			
		} catch (SQLException e) {
			log.error("Error deleting subscriptions.", e);
            e.printStackTrace();
            return false;
		}
		

	}
}
